<?php
$severname = "localhost";
$username = "root";
$password = "";
$dbname = "bookstore_db";

//Create connection
$conn = new mysqli($severname, $username, $password, $dbname);
//Check connection
if ($conn->connect_error) {
    exit("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT COUNT(*) FROM staffs";
$result = $conn->query($sql);

if($result->num_rows > 0) {
    $staffs_a = $result->fetch_array();
    echo "The current amount of staffs in book store is " . $staffs_a[0] . " people.";
} else {
    echo "Error: " . $conn->error;
}

$conn->close();
?>