<!DOCTYPE html>
<html>
<head>
<style>
    table, th, td {
        border: 1px solid black;
    }

    th {
        background-color: #98bfe3;
    }
</style>
</head>

<body>

<?php
$severname = "localhost";
$username = "root";
$password = "";
$dbname = "bookstore_db";

//Create connection
$conn = new mysqli($severname, $username, $password, $dbname);
//Check connection
if ($conn->connect_error) {
    exit("Connection failed: " . $conn->connect_error);
}

$sql_s = "SELECT * FROM staffs ORDER BY id";
$result_s = $conn->query($sql_s);

$sql_b = "SELECT * FROM books ORDER BY reg_date";
$result_b = $conn->query($sql_b);

$sql_p = "SELECT * FROM books_purchase";
$result_p = $conn->query($sql_p);

if($result_s->num_rows > 0) {
    echo "<table><tr><th>Staff ID</th><th>First name</th><th>Last name</th><th>Age</th><th>Address</th><th>Reg-Date</th></tr>";
    //display data in loop
    while($row = $result_s->fetch_assoc()) {
        echo "<tr><td>" . $row["id"] . "</td><td>" . $row["firstname"] . "</td><td>" . $row["lastname"] . "</td><td>" . $row["age"] . "</td><td>" 
        . $row["address"] . "</td><td>" . $row["reg_date"] . "</td></tr>";
    }
    echo "</table>";
} else {
    echo "0 results";
}

if($result_b->num_rows > 0) {
    echo "<table><tr><th>ISBN</th><th>Title</th><th>Price</th><th>Reg-Date</th></tr>";
    //display data in loop
    while($row = $result_b->fetch_assoc()) {
        echo "<tr><td>" . $row["ISBN"] . "</td><td>" . $row["bookname"] . "</td><td>" . $row["price"] . "</td><td>" . $row["reg_date"] . "</td></tr>";
    }
    echo "</table>";
} else {
    echo "0 results";
}

if($result_p->num_rows > 0) {
    echo "<table><tr><th>ISBN</th><th>Amount</th></tr>";
    //display data in loop
    while($row = $result_p->fetch_assoc()) {
        echo "<tr><td>" . $row["ISBN"] . "</td><td>" . $row["amount"] . "</td></tr>";
    }
    echo "</table>";
} else {
    echo "0 results";
}


$conn->close();
?>

</body>
</html>