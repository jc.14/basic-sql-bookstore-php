<?php
$severname = "localhost";
$username = "root";
$password = "";

//Create connection
$conn = new mysqli($severname, $username, $password);
//Check connection
if ($conn->connect_error) {
    exit("Connection failed: " . $conn->connect_error);
}

//Create Database
$sql = "CREATE DATABASE bookstore_db";
if($conn->query($sql) === TRUE) {
    echo "Database \"bookstore_db\" created successfully";
} else {
    echo "Error creating database: " . $conn->error;
}

$conn->close();
?>