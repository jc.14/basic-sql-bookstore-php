<!DOCTYPE html>
<html>
<head>
<style>
    table, th, td {
        border: 1px solid black;
    }

    th {
        background-color: #98bfe3;
    }
</style>
</head>

<body>

<?php
$severname = "localhost";
$username = "root";
$password = "";
$dbname = "bookstore_db";

//Create connection
$conn = new mysqli($severname, $username, $password, $dbname);
//Check connection
if ($conn->connect_error) {
    exit("Connection failed: " . $conn->connect_error);
}

$sql_s = "SELECT firstname , lastname FROM staffs WHERE age < 20";
$result_s = $conn->query($sql_s);

if($result_s->num_rows > 0) {
    echo "<h3>List of staffs' name under 20 years old</h3>";
    //display data in loop
    $num = 1;
    while($row = $result_s->fetch_assoc()) {
        echo $num . ". " . $row["firstname"] . " " . $row["lastname"] . "<br>";
        $num++;
    }
} else {
    echo "0 results";
}

$conn->close();
?>

</body>
</html>