<?php
$severname = "localhost";
$username = "root";
$password = "";
$dbname = "bookstore_db";

//Create connection
$conn = new mysqli($severname, $username, $password, $dbname);
//Check connection
if ($conn->connect_error) {
    exit("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT SUM(books_purchase.amount * books.price) FROM books_purchase JOIN books ON books_purchase.ISBN = books.ISBN";
$result = $conn->query($sql);

if($result->num_rows > 0) {
    $sum = $result->fetch_array();
    echo "Total income amount is " . $sum[0] . ".";
} else {
    echo "Error: " . $conn->error;
}

$conn->close();
?>