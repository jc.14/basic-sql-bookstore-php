<!DOCTYPE html>
<html>
<head>
<style>
    table, th, td {
        border: 1px solid black;
    }

    th {
        background-color: #98bfe3;
    }
    table {
        display: table-cell;
    }
</style>
</head>

<body>

<?php
$severname = "localhost";
$username = "root";
$password = "";
$dbname = "bookstore_db";

//Create connection
$conn = new mysqli($severname, $username, $password, $dbname);
//Check connection
if ($conn->connect_error) {
    exit("Connection failed: " . $conn->connect_error);
}

/*-------------------Display all staffs-------------------------*/
$sql_a = "SELECT firstname, age FROM staffs ORDER BY id";
$result_a = $conn->query($sql_a);

if($result_a->num_rows > 0) {
    echo "<table><tr><th colspan=2>Staff of bookstore</th></tr>";
    echo "<tr><th>Name</th><th>Age</th></tr>";
    //display data in loop
    while($row = $result_a->fetch_assoc()) {
        echo "<tr><td>" . $row["firstname"] . "</td><td>" . $row["age"] . "</td></tr>";
    }
    echo "</table>";
} else {
    echo "0 results";
}

/*----------------Display staffs only under 20 years old------------------*/
$sql_s = "SELECT firstname , lastname FROM staffs WHERE age < 20";
$result_s = $conn->query($sql_s);

if($result_s->num_rows > 0) {

    echo "<table><tr><th>Staffs under 20 years old</th></tr>";
    //display data in loop
    while($row = $result_s->fetch_assoc()) {
        echo "<tr><td>" . $row["firstname"] . " " . $row["lastname"] . "</td></tr>";
    }
    echo "</table>";
} else {
    echo "0 results";
}

/*--------------Count amount of staffs------------------*/
$sql = "SELECT COUNT(*) FROM staffs";
$result = $conn->query($sql);

if($result->num_rows > 0) {
    $staffs_a = $result->fetch_array();
    echo "<br>The current amount of staffs in book store is <strong>" . $staffs_a[0] . "</strong> people.";
} else {
    echo "Error: " . $conn->error;
}

$conn->close();
?>

</body>
</html>