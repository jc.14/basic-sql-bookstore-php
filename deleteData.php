<?php
$severname = "localhost";
$username = "root";
$password = "";
$dbname = "bookstore_db";

//Create connection
$conn = new mysqli($severname, $username, $password, $dbname);
//Check connection
if ($conn->connect_error) {
    exit("Connection failed: " . $conn->connect_error);
}

$sql = "DELETE FROM staffs WHERE firstname = 'Cameron' AND lastname = 'Moore'";

if($conn->query($sql) === TRUE) {
    echo "Data removed successfully";
} else {
    echo "Error: " . $conn->error;
}

$conn->close();
?>